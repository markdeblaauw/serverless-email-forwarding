import unittest

import boto3
from dotenv import load_dotenv
from moto import mock_ses

from serverless_email_forwarding.runtime.email_forwarding.forward import send_email_with_ses, transform_email

# Load test configuration
load_dotenv("tests/config/test_config.env")


class TestEmailForwarding(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        with open(
            "tests/example_data/emails/test_email",
            "rb",
        ) as file_object:
            cls.email = file_object.read().decode("UTF-8")

        cls.mock_ses = mock_ses()
        cls.mock_ses.start()
        cls.ses_client = boto3.client("ses")

        # Add email address to ses
        cls.ses_client.verify_email_identity(EmailAddress="info@example.com")

    @classmethod
    def tearDownClass(cls):
        cls.mock_ses.stop()

    def test_dummy(self):
        transform_email(email=self.email, from_address="dummy@dum.com")

    def test_transform_email_updates(self):
        with open("tests/example_data/emails/test_email.txt", "rb") as file_object:
            raw_email = file_object.read().decode("UTF-8")

        with open("tests/example_data/emails/transformed_email.txt", "rb") as file_object:
            expected_email = file_object.read().decode("UTF-8")

        output = transform_email(email=raw_email, from_address="info@example.com")

        self.assertEqual(expected_email, output)

    def test_transform_email_preserve_reply_to(self):
        with open("tests/example_data/emails/email_replyto.txt", "rb") as file_object:
            raw_email = file_object.read().decode("UTF-8")

        with open(
            "tests/example_data/emails/transformed_email.txt",
            "rb",
        ) as file_object:
            expected_email = file_object.read().decode("UTF-8")

        output = transform_email(email=raw_email, from_address="info@example.com")

        self.assertEqual(expected_email, output)

    def test_transform_email_preserver_reply_to_lowercase(self):
        with open(
            "tests/example_data/emails/email_replyto_case.txt",
            "rb",
        ) as file_object:
            raw_email = file_object.read().decode("UTF-8")

        with open(
            ("tests/example_data/emails" "/transformed_email_replyto_case.txt"),
            "rb",
        ) as file_object:
            expected_email = file_object.read().decode("UTF-8")

        output = transform_email(email=raw_email, from_address="info@example.com")

        self.assertEqual(expected_email, output)

    def test_transform_email_override_all_from_headers(self):
        with open(
            "tests/example_data/emails/email_from_multiline.txt",
            "rb",
        ) as file_object:
            raw_email = file_object.read().decode("UTF-8")

        with open(
            ("tests/example_data/emails" "/transformed_email_from_multiline.txt"),
            "rb",
        ) as file_object:
            expected_email = file_object.read().decode("UTF-8")

        output = transform_email(email=raw_email, from_address="noreply@example.com")

        self.assertEqual(expected_email, output)

    def test_send_email_with_ses(self):
        with open(
            "tests/example_data/emails/transformed_email.txt",
            "rb",
        ) as file_object:
            transformed_email = file_object.read().decode("UTF-8")

        send_email_with_ses(
            from_address="info@example.com",
            recipient_address="test@example.com",
            email_data=transformed_email,
            ses_client=self.ses_client,
        )

        self.assertTrue(int(self.ses_client.get_send_quota()["SentLast24Hours"]) > 0)

    def test_send_email_with_ses_wrong_identity(self):
        with open(
            "tests/example_data/emails/transformed_email.txt",
            "rb",
        ) as file_object:
            transformed_email = file_object.read().decode("UTF-8")

        self.assertRaises(
            self.ses_client.exceptions.MessageRejected,
            send_email_with_ses,
            from_address="info2@example.com",
            recipient_address="test@example.com",
            email_data=transformed_email,
            ses_client=self.ses_client,
        )
