import unittest

import boto3
from dotenv import load_dotenv
from moto import mock_s3

from serverless_email_forwarding.runtime.email_forwarding.utils import read_txt_from_s3

# Load test configuration
load_dotenv("tests/config/test_config.env")


class TestUtils(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.mock_s3 = mock_s3()
        cls.mock_s3.start()

        cls.s3_client = boto3.client("s3")

        cls.email_bucket = "email_bucket"
        cls.s3_client.create_bucket(Bucket=cls.email_bucket)

        with open("tests/example_data/emails/test_email", "rb") as file_object:
            cls.s3_client.put_object(
                Body=file_object,
                Bucket=cls.email_bucket,
                Key="first_email",
                ContentType="application/octet-stream",
            )

        with open("tests/example_data/emails/test_email", "rb") as file_object:
            cls.email = file_object.read().decode("UTF-8")

    @classmethod
    def tearDownClass(cls):
        cls.mock_s3.stop()

    def test_read_txt_from_s3(self):
        output = read_txt_from_s3(
            bucket=self.email_bucket,
            key="first_email",
            s3_client=self.s3_client,
        )

        self.assertEqual(output, self.email)

    def test_read_txt_from_s3_no_file(self):
        self.assertRaises(
            self.s3_client.exceptions.NoSuchKey,
            read_txt_from_s3,
            bucket=self.email_bucket,
            key="fake_email",
            s3_client=self.s3_client,
        )
