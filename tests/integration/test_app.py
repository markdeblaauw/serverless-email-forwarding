import os
import unittest
from unittest import mock

import boto3
from dotenv import load_dotenv
from moto import mock_s3, mock_ses

# Load test configuration
load_dotenv("tests/config/test_config.env")


@mock.patch.dict(
    os.environ,
    {
        "from_address": "info@example.com",
        "new_recipient_address": "dummy@dummy.com",
    },
)
class TestLambdaFunction(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.mock_s3 = mock_s3()
        cls.mock_ses = mock_ses()

        cls.mock_s3.start()
        cls.mock_ses.start()

        cls.s3_client = boto3.client("s3")
        cls.ses_client = boto3.client("ses")

        cls.email_bucket = "email-bucket"
        cls.s3_client.create_bucket(Bucket=cls.email_bucket)

        with open("tests/example_data/emails/test_email.txt", "rb") as file_object:
            cls.s3_client.put_object(
                Body=file_object,
                Bucket=cls.email_bucket,
                Key="ncp3phvjgl4i3dstpn55b299ponmgb77s4lrn181",
                ContentType="application/octet-stream",
            )

        cls.ses_client.verify_email_identity(EmailAddress="info@example.com")

        cls.test_event = {
            "Records": [
                {
                    "eventVersion": "2.1",
                    "eventSource": "aws:s3",
                    "awsRegion": "eu-west-1",
                    "eventTime": "2021-09-09T14:39:40.227Z",
                    "eventName": "ObjectCreated:Put",
                    "userIdentity": {"principalId": "AWS:AIDAJ"},
                    "requestParameters": {"sourceIPAddress": "10.90.155.122"},
                    "responseElements": {
                        "x-amz-request-id": "ETZ8K4DXZ8K0AW1V",
                        "x-amz-id-2": ("9HbD7jyo48Pt+u9SletbLPcioPSFVqKFzXAbnZv8NeO"),
                    },
                    "s3": {
                        "s3SchemaVersion": "1.0",
                        "configurationId": ("1c9652eb-fee8-4906-b240-8883839e1443"),
                        "bucket": {
                            "name": "email-bucket",
                            "ownerIdentity": {"principalId": "12345"},
                            "arn": "arn:aws:s3:::email-bucket",
                        },
                        "object": {
                            "key": "ncp3phvjgl4i3dstpn55b299ponmgb77s4lrn181",
                            "size": 6981,
                            "eTag": "e019359b9584db7c89e486285c976baa",
                            "sequencer": "00613A1CB2222E4874",
                        },
                    },
                }
            ]
        }

    @classmethod
    def tearDownClass(cls):
        cls.mock_s3.stop()
        cls.mock_ses.stop()

    def test_general_lambda_handler(self):
        from serverless_email_forwarding.runtime.app import handler

        handler(event=self.test_event, context={})

        self.assertTrue(int(self.ses_client.get_send_quota()["SentLast24Hours"]) > 0)
