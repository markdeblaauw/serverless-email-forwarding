# Variable setting, e.g., make synth STAGE=prod
STAGE?=dev

.PHONY: tests coverage synth deploy diff destroy

install_cdk:
	npm ci

local-venv:
	python3 -m venv venv

install_packages:
	python -m pip install --upgrade pip
	pip install pip-tools==6.5.1
	pip-sync requirements.txt \
	serverless_email_forwarding/runtime/requirements.txt

	pre-commit install

install_dev_packages:
	python -m pip install --upgrade pip
	pip install pip-tools==6.5.1
	pip-sync requirements.txt requirements-dev.txt \
	serverless_email_forwarding/runtime/requirements.txt

	pre-commit install

compile_python_requirements:
	pip-compile serverless_email_forwarding/runtime/requirements.in
	pip-compile requirements.in
	pip-compile requirements-dev.in

upgrade_node_requirements:
	npm install

upgrade_python_requirements:
	pip-compile --upgrade serverless_email_forwarding/runtime/requirements.in
	pip-compile --upgrade requirements.in
	pip-compile --upgrade requirements-dev.in

pre_commit:
	pre-commit run --all-files

tests:
	. config/paths.sh && \
	python -m unittest discover -s tests -p "test_*.py" -b

coverage:
	. config/paths.sh && \
	coverage run -m unittest discover -s tests -p "test_*.py" -b

coverage_report:
	. config/paths.sh && \
	coverage run -m unittest discover -s tests -p "test_*.py" -b
	coverage html
	open htmlcov/index.html

# e.g., `make synth STAGE=prod`
synth:
	@npx cdk synth -c stage=$(STAGE)

deploy: synth
	@npx cdk deploy email-forwarding-$(STAGE)/* -c stage=$(STAGE) --require-approval never

diff:
	@npx cdk diff -c stage=$(STAGE)

destroy:
	@npx cdk destroy -c stage=$(STAGE)

bootstrapp-cdk-toolkit:
	# Get deployment region from env file.
	# Then get Account id.
	export $(shell cat config/$(STAGE).env | xargs); \
	ACCOUNT=$(shell aws sts get-caller-identity --query Account --output text); \
	npx cdk bootstrap $$ACCOUNT/$$CDK_REGION -c stage=$(STAGE)