"""This script creates the backend."""
from typing import Any

import aws_cdk as cdk
import constructs

from serverless_email_forwarding.infrastructure import ServerlessEmailForwardingStack


class Backend(cdk.Stage):
    """Create the backend."""

    def __init__(self, scope: constructs.Construct, id: str, **kwargs: Any) -> None:
        """Initialise the Backend class.

        Args:
            scope (cdk.Construct): Parent construct.
            id (str): Identifier.
        """
        super().__init__(scope, id, **kwargs)

        app_name = self.node.try_get_context("application_name")
        stage = self.node.try_get_context("stage")

        # Create core application backend.
        application = cdk.Stack(self, "application", description="Backend infrastructure.")

        ServerlessEmailForwardingStack(application, "BackendOfApplication", app_name=app_name, stage=stage)
