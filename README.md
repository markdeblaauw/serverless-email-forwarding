
# serverless-email-forwarding

This implementation is inspired by this [blogpost](https://infra.engineer/aws/11-using-gmail-with-your-domain-utilising-aws-ses-lambda) and this [Github](https://github.com/arithmetric/aws-lambda-ses-forwarder) repository. The Lambda function code in this repository is simplified and written in Python. The repository also provides CDK code to create the service with AWS CloudFormation.

## AWS Architecture

![Alt text](images/serverless_email_forwarding.jpg)

This repository does not set-up Route53 and does not verify the incoming and outgoing domains in SES.

## How to setup locally?

### Requirements:
1. Have Docker, Python3.8, nodejs and make.
2. Run `make install_cdk` to install CDK CLI.
3. `make local-venv` and `source venv/bin/activate`.
4. `make install_dev_packages`.

You can now run `make tests` or create a local AWS CloudFormation stack with `make synth`.
