"""This function takes forwarded emails from S3 and sends them with SES to a new recipient."""
import os
from typing import Any, Dict

import boto3
from email_forwarding.forward import send_email_with_ses, transform_email
from email_forwarding.utils import read_txt_from_s3

S3_CLIENT = boto3.client("s3")
SES_CLIENT = boto3.client("ses")


def handler(event: Dict[str, Any], context: Dict[str, Any]) -> None:
    """Gets triggered by an email that is put into the S3 bucket.

    Args:
        event (Dict[str, Any]): Information about the event that triggers the lambda function.
        context (Dict[str, Any]): Information about the status of the lambda function.
    """
    bucket = event["Records"][0]["s3"]["bucket"]["name"]
    key = event["Records"][0]["s3"]["object"]["key"]

    raw_email = read_txt_from_s3(bucket=bucket, key=key, s3_client=S3_CLIENT)

    transformed_email = transform_email(email=raw_email, from_address=os.environ["from_address"])

    send_email_with_ses(
        from_address=os.environ["from_address"],
        recipient_address=os.environ["new_recipient_address"],
        email_data=transformed_email,
        ses_client=SES_CLIENT,
    )
