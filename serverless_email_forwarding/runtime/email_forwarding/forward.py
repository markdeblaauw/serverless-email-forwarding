"""Provides functions to transform and send the email."""
import logging
import re

from mypy_boto3_ses import SESClient


def transform_email(email: str, from_address: str) -> str:
    """This functions transforms the email.

    It transforms it so that it can be forwarded with SES to a new recipient.

    Args:
        email (str): The raw email data.
        from_address (str): The new from address, which is verified in SES.

    Returns:
        str: The transformed email.
    """
    # Split email into header and body.
    match = re.search(
        r"^((?:[^\n\r]+\r?\n)*)(\r?\n(?:[^\n\r]*\s+)*)",
        email,
        flags=re.MULTILINE,
    )

    header = match.group(1)
    body = match.group(2)

    # If no reply-to header add it.
    if not re.search(r"^reply-to:[\t ]?", header.lower(), flags=re.MULTILINE | re.IGNORECASE):
        match = re.search(
            r"^from:[\t ]?([^\n\r]*(?:\r?\n\s+[^\n\r]*)*\r?\n)",
            header,
            flags=re.MULTILINE | re.IGNORECASE,
        )
        email_from = match.group(1)

        if email_from:
            header = header + "Reply-To: " + email_from
        else:
            logging.warning("Reply-To not added, because could not find From adress")

    # Change the from address.
    pattern = re.compile(
        r"^from:[\t ]?([^\n\r]*(?:\r?\n\s+[^\n\r]*)*)",
        flags=re.MULTILINE | re.IGNORECASE,
    )
    email_from = re.search(pattern, header).group(1)
    email_from_stripped = re.sub(r"<(.*)>", "", email_from).strip()
    header = re.sub(pattern, f"From: {email_from_stripped} <{from_address}>", header)

    # Remove the Return-Path header.
    header = re.sub(
        r"^return-path:[\t ]?([^\n\r]*)\r?\n",
        "",
        header,
        flags=re.MULTILINE | re.IGNORECASE,
    )

    # Remove Sender header.
    header = re.sub(
        r"^sender:[\t ]?([^\n\r]*)\r?\n",
        "",
        header,
        flags=re.MULTILINE | re.IGNORECASE,
    )

    # Remove Message-ID header.
    header = re.sub(
        r"^message-id:[\t ]?([^\n\r]*)\r?\n",
        "",
        header,
        flags=re.MULTILINE | re.IGNORECASE,
    )

    # Remove all DKIM-Signature headers to prevent triggering an
    # "InvalidParameterValue: Duplicate header 'DKIM-Signature'" error.
    # These signatures will likely be invalid anyways, since the From
    # header was modified.
    header = re.sub(
        r"^dkim-signature:[\t ]?[^\n\r]*\r?\n(\s+[^\n\r]*\r?\n)*",
        "",
        header,
        flags=re.MULTILINE | re.IGNORECASE,
    )

    return header + body


def send_email_with_ses(from_address: str, recipient_address: str, email_data: str, ses_client: SESClient) -> None:
    """Send an email using SES.

    Args:
        from_address (str): The address from which the email should be send.
        recipient_address (str): The email address too which the email should be send.
        email_data (str): Email data that must include headers and a body.
        ses_client (SESClient): Boto3 SES SDK.

    Raises:
        e: Errors with sending the email using the SES client.
    """
    try:
        ses_client.send_raw_email(
            Source=from_address,
            Destinations=[
                recipient_address,
            ],
            RawMessage={"Data": email_data},
        )
    except Exception as e:
        logging.warning(f"Error sending email from {from_address} to {recipient_address}")
        raise e
