"""Provides utility functionality for the package."""
import logging

from mypy_boto3_s3 import S3Client


def read_txt_from_s3(bucket: str, key: str, s3_client: S3Client) -> str:
    """[Read plain text file from an S3 bucket and decode as string.

    Args:
        bucket (str): The bucket where the txt file is.
        key (str): The key of the text file.
        s3_client (S3Client): Boto3 S3 SDK.

    Raises:
        e: Error reading text file from S3.

    Returns:
        str: The text file as string from S3.
    """
    try:
        response = s3_client.get_object(Bucket=bucket, Key=key)
    except Exception as e:
        logging.warning(f"Error getting object {key} from bucket {bucket}.")
        raise e
    content = response["Body"].read().decode("UTF-8")
    response["Body"].close()
    return content
