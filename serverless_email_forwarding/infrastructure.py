"""Provides the code for the serverless email forwarding stack."""
import os

import aws_cdk as cdk
import constructs

# from aws_cdk import aws_lambda_python as _lambda
# from aws_cdk import aws_lambda as _lambda
from aws_cdk import aws_iam as iam
from aws_cdk import aws_lambda_python_alpha as _lambda
from aws_cdk import aws_s3 as s3
from aws_cdk import aws_s3_notifications as s3n
from aws_cdk import aws_ses as ses
from aws_cdk import aws_ses_actions as actions

# from aws_cdk import core as cdk
from aws_cdk.aws_lambda import Runtime


class ServerlessEmailForwardingStack(constructs.Construct):
    def __init__(
        self,
        scope: constructs.Construct,
        id: str,
        app_name: str,
        stage: str,
    ) -> None:
        """Initialise the serverless email forwarding construct with parameters.

        This stack implements a serverless email forwarding service in AWS. It uses
        SES to receive and resend emails.

        Args:
            scope (constructs.Construct): Parent construct.
            id (str): Identifier.
            app_name (str): The name of the application in which this construct
                is build.
            stage (str): The stage in which this construct is part of. This can be either
                dev, test or prod.
        """
        super().__init__(scope, id)

        # Bucket in which incoming emails from SES will be dumped.
        email_bucket = s3.Bucket(
            self,
            auto_delete_objects=True,
            id="emailBucket",
            lifecycle_rules=[s3.LifecycleRule(enabled=True, expiration=cdk.Duration.days(1))],
            removal_policy=cdk.RemovalPolicy.DESTROY,
        )

        # SES rule that forwards emails to the S3 bucket.
        ses.ReceiptRuleSet(
            self,
            id="emailForwarding",
            receipt_rule_set_name=f"{app_name}-rule-set-{stage}",
            rules=[
                ses.ReceiptRuleOptions(recipients=[os.getenv("RECIPIENT")], actions=[actions.S3(bucket=email_bucket)])
            ],
        )

        # Lambda that receives emails, changes headers, and
        # send them with SES.
        lambda_email_forwarder = _lambda.PythonFunction(
            self,
            id="emailForwarder",
            function_name=f"{app_name}-lambda-{stage}",
            entry="serverless_email_forwarding/runtime",
            index="app.py",
            runtime=Runtime.PYTHON_3_8,
            environment={"from_address": os.getenv("RECIPIENT"), "new_recipient_address": os.getenv("NEW_RECIPIENT")},
            timeout=cdk.Duration.seconds(360),
        )
        # Example with own Docker.
        # lambda_email_forwarder = _lambda.Function(
        #     self,
        #     id="emailForwarder",
        #     code=_lambda.Code.from_asset(
        #         path=os.path.join(os.path.dirname(__file__), "runtime"),
        #         bundling=cdk.BundlingOptions(
        #             image=cdk.DockerImage.from_build(
        #                 path=os.path.join(os.path.dirname(__file__)),
        #             ),
        #             command=["cp", "-R", "/asset-stage/runtime", "/asset-output"],
        #         ),
        #     ),
        #     function_name=f"{app_name}-lambda-{stage}",
        #     handler="app.handler",
        #     runtime=Runtime.PYTHON_3_8,
        #     environment={"from_address": os.getenv("RECIPIENT"), "new_recipient_address": os.getenv("NEW_RECIPIENT")},
        #     timeout=cdk.Duration.seconds(360),
        # )

        # Lambda policies.
        # Allow Lambda to send emails with SES.
        lambda_email_forwarder.add_to_role_policy(
            statement=iam.PolicyStatement(actions=["ses:SendRawEmail"], resources=["*"], effect=iam.Effect.ALLOW)
        )

        # Allow Lambda to get emails from the email bucket.
        lambda_email_forwarder.add_to_role_policy(
            statement=iam.PolicyStatement(
                actions=["s3:GetObject", "s3:ListBucket"],
                resources=[email_bucket.arn_for_objects(key_pattern="*"), email_bucket.bucket_arn],
                effect=iam.Effect.ALLOW,
            )
        )

        # Triggers event to AWS Lambda when email is put in S3 bucket.
        email_bucket.add_event_notification(
            event=s3.EventType.OBJECT_CREATED_PUT, dest=s3n.LambdaDestination(lambda_email_forwarder)
        )
